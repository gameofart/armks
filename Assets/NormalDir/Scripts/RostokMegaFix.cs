﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RostokMegaFix : MonoBehaviour
{
    public float _targetScale = 0.5f;
    private Vector3 startParentScale;

    private void Awake()
    {
        startParentScale = this.transform.parent.localScale;
    }

    private void OnEnable()
    {
        this.transform.parent.localScale = startParentScale;
    }

    public void MakeFix ()
    {
        this.transform.parent.localScale = _targetScale * Vector3.one;
    }
}
