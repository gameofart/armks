﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppleDrop : MonoBehaviour
{
    public GameObject _Apple;
    public GameObject[] _SplitApple;

    private void OnEnable()
    {
        _Apple.SetActive(true);
        _SplitApple[0].SetActive(false);
        _SplitApple[1].SetActive(false);
    }

    public void Drop ()
    {
        _Apple.SetActive(false);
        _SplitApple[0].SetActive(true);
        _SplitApple[1].SetActive(true);
    }
}
