﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Anim_Event : MonoBehaviour
{
    [System.Serializable]
    public class AEvent : UnityEvent{}
    public AEvent OnEvent;

    public void Action ()
    {
        OnEvent?.Invoke();
    }
}
