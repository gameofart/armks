﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageActivate : MonoBehaviour
{
    public GameObject _panel;

    public void ChangeActivation()
    {
        _panel.SetActive(!_panel.activeSelf);
    }
}
