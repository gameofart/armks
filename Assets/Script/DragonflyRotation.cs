﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonflyRotation : MonoBehaviour {
    public GameObject center;
	public Animator ar;
	public bool arb;
	// Use this for initialization
	void Start () {
		
    ar.Play("Scene");
	}
	 void OnEnable()
    {
       
    }
	// Update is called once per frame
	void Update () {
		if (arb==false)

		{
        this.transform.RotateAround(center.transform.position,-center.transform.up*0.01f, 100*Time.deltaTime);
		}
		else
		        this.transform.RotateAround(center.transform.position,center.transform.up*0.01f, 100*Time.deltaTime);
	}
}
