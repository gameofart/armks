﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour
{
    public Image _soundImage;

    public Sprite _on, _off;
    private bool _isOn = true;

    public void Change()
    {
        _isOn = !_isOn;
        if (_isOn) _soundImage.sprite = _on;
        else _soundImage.sprite = _off;
    }
}
